#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFont buttonFont = QFont("Times", 16, QFont::Bold);
    QSize buttonSize = QSize(50,50);

    ui->pushButton->setFont(buttonFont);
    ui->pushButton->setIcon(QIcon("://icons/lec.png"));
    ui->pushButton->setIconSize(buttonSize);
    ui->pushButton_2->setFont(buttonFont);
    ui->pushButton_2->setIcon(QIcon("://icons/mod.png"));
    ui->pushButton_2->setIconSize(buttonSize);
    ui->pushButton_3->setFont(buttonFont);
    ui->pushButton_3->setIcon(QIcon("://icons/die.png"));
    ui->pushButton_3->setIconSize(buttonSize);
    ui->pushButton_4->setFont(buttonFont);
    ui->pushButton_4->setIcon(QIcon("://icons/conf.png"));
    ui->pushButton_4->setIconSize(buttonSize);

    QFile file("://lectures/test.html");
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(0, "Error Lectures", file.errorString());
    }
    else
    {
        QString html(file.readAll());
        ui->textBrowser->setFont(QFont("Times", 14));
        ui->textBrowser->setHtml(html);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page_2);
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page_3);
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page_4);
}
