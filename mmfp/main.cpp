#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowIcon(QIcon(":/icons/main.png"));
    w.setStyleSheet("QMainWindow {background: 'darkgrey';}");
    w.show();
    return a.exec();
}
